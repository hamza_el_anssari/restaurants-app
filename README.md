### Restaurant Rest API ###

The goal of this project is to develop a Rest API that allows users to order online in restaurants.

This project will help users find restaurants that match their needs (category, price ,.…). Other features are also added, such as:

* Consult the menu of a restaurant
* Order and Post Reviews
* Manage orders
* Etc ..


In addition, this application gives the possibility to administrators to manage their restaurants.


### Class Diagram ###

![Class Diagram](https://bitbucket.org/hamza_el_anssari/restaurants-app/raw/dfaf95a0ba75038c8e5b5480cc55dccb90367ea2/class_diagram.png)


### Quick Start ###

* Clone the repository: git clone https://Hamza_El_Anssari@bitbucket.org/hamza_el_anssari/restaurants-app.git
* Change DATABASES configuration in api/settings.py file + Create your postgres database
* Run pip install -r ./api/requirements.txt
* Run python manage.py makemigrations authentications orders restaurants
* Run python manage.py migrate
* Run python manage.py runserver
* Check http://localhost:8000/


### Using Docker ###

* Clone the repository: git clone https://Hamza_El_Anssari@bitbucket.org/hamza_el_anssari/restaurants-app.git
* Run docker-compose up