from django.db import models
from authentications.models import Manager, User
from django.core.validators import MaxValueValidator, MinValueValidator


class Restaurant(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField(blank=True, null=True)
    address = models.CharField(max_length=300, blank=True, null=True)
    country = models.CharField(max_length=150, blank=True, null=True)
    city = models.CharField(max_length=150, blank=True, null=True)
    phone = models.CharField(max_length=15, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    manager = models.ForeignKey(Manager, related_name='restaurants', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=150)
    restaurant = models.ForeignKey(Restaurant, related_name='categories', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Food(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField(blank=True, null=True)
    ingredients = models.TextField(blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    category = models.ForeignKey(Category, related_name="foods", on_delete=models.CASCADE)
    restaurant = models.ForeignKey(Restaurant, related_name="foods", on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Review(models.Model):
    body = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    rate = models.IntegerField(
        default=1,
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1)
        ]
     )
    choices = (
    ('Positive', 'Positive'),
    ('Negative', 'Negative'),
    )
    type = models.CharField(max_length=100, choices=choices, blank=True)
    restaurant = models.ForeignKey(Restaurant, related_name="reviews", on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name="reviews", on_delete=models.CASCADE)

    def __str__(self):
        return self.rate