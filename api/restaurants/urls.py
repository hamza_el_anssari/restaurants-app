from django.urls import path
from .views import *


urlpatterns = [
    #     "------------------------- Restaurant urls -------------------------"

    #     "----------For Users ----------"
    path('restaurants/', ListRestaurantsApiView.as_view(), name='restaurants'),
    path('restaurants/<int:pk>/', GetRestaurantApiView.as_view(), name='get-restaurant'),

    #     "----------For Admins ----------"
    path('add-restaurant/', AddRestaurantApiView.as_view(), name='add-restaurant'),
    path('restaurants/me/', ListMyRestaurantsApiView.as_view(), name='my-restaurants'),
    path('restaurants/me/<int:pk>/', Get_Update_Delete_My_RestaurantApiView.as_view(), name='my-restaurants'),


    #     "------------------------- Category urls -------------------------"

    #     "----------For Users ----------"
    path('categories/', ListFoodCategoryApiView.as_view(), name='categories'),
    path('categories/<int:pk>/', GetFoodCategoryApiView.as_view(), name='get-category'),
    path('restaurant-categories/<int:pk>/', ListFoodCategoryByRestaurantApiView.as_view(), name='restaurants-categories'),

    #     "----------For Admins ----------"
    path('add-category/', AddFoodCategoryApiView.as_view(), name='add-category'),
    path('categories/me/', ListMyFoodCategoryApiView.as_view(), name='my-categories'),
    path('restaurant-categories/me/<int:pk>/', ListMyRestaurantFoodCategoryApiView.as_view(), name='my-categories'),
    path('categories/me/<int:pk>/', Get_Update_Delete_My_CategoryApiView.as_view(), name='get-update-delete-my-category'),

    #     "------------------------- Food urls -------------------------"

    #     "----------For Users ----------"
    path('foods/', ListFoodApiView.as_view(), name='foods'),
    path('foods-category/<int:pk>/', ListFoodByCategoryApiView.as_view(), name='foods-by-category'),
    path('foods-restaurant/<int:pk>/', ListFoodByRestaurantApiView.as_view(), name='foods-by-restaurants'),
    path('foods/<int:pk>/', GetFoodApiView.as_view(), name='get-food'),

    #     "----------For Admins ----------"

    # Check if category in restaurant categories
    path('add-food/', AddFoodApiView.as_view(), name='add-food'),
    path('foods/me/', ListMyFoodApiView.as_view(), name='my-foods'),
    path('foods/me/<int:pk>/', Get_Update_Delete_My_FoodApiView.as_view(), name='get-update-delete-my-food'),

    #     "------------------------- Review urls -------------------------"

    #     "----------For Users ----------"
    path('reviews/restaurant/<int:pk>/', ListReviewsByRestaurantApiView.as_view(), name='reviews'),
    path('reviews/<int:pk>/', GetReviewApiView.as_view(), name='get-review'),
    path('reviews/me/', ListMyReviewsApiView.as_view(), name='my-reviews'),
    path('add-review/', AddReviewApiView.as_view(), name='add-review'),
    path('reviews/me/<int:pk>/', Get_Update_Delete_My_ReviewApiView.as_view(), name='get-update-delete-my-reviews'),


]
