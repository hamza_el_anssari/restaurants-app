from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView
from rest_framework.views import APIView
from .serializers import *
from authentications.permissions import IsManager
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from .models import *
from authentications.models import Manager
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import joblib
import sklearn
from django.db.models import Count
"------------------------- Restaurant Views -------------------------"


class AddRestaurantApiView(CreateAPIView):
    serializer_class = RestaurantSerializer
    permission_classes = [IsAuthenticated, IsManager]

    def perform_create(self, serializer):
        manager = Manager.objects.get(user=self.request.user)
        serializer.save(manager=manager)


class ListRestaurantsApiView(ListAPIView):
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()


class GetRestaurantApiView(RetrieveAPIView):
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()


class ListMyRestaurantsApiView(ListAPIView):
    serializer_class = RestaurantSerializer
    permission_classes = [IsAuthenticated, IsManager]

    def get_queryset(self, user):
        manager = Manager.objects.get(user=user)
        queryset = Restaurant.objects.filter(manager=manager)
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset(request.user)
        serializer = RestaurantSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)





class Get_Update_Delete_My_RestaurantApiView(RetrieveUpdateDestroyAPIView):
    serializer_class = RestaurantSerializer
    permission_classes = [IsAuthenticated, IsManager]
    def get_queryset(self):
        manager = Manager.objects.get(user=self.request.user)
        queryset = Restaurant.objects.filter(manager=manager)
        return queryset


"------------------------- Category Views -------------------------"


class ListFoodCategoryApiView(ListAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class GetFoodCategoryApiView(RetrieveAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class ListFoodCategoryByRestaurantApiView(ListAPIView):
    serializer_class = CategorySerializer

    def list(self, request, *args, **kwargs):
        queryset = Category.objects.filter(restaurant=self.kwargs['pk'])
        serializer = CategorySerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class AddFoodCategoryApiView(CreateAPIView):
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated, IsManager]


class Get_Update_Delete_My_CategoryApiView(RetrieveUpdateDestroyAPIView):
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated, IsManager]

    def get_queryset(self):
        manager = Manager.objects.get(user=self.request.user)
        restaurant = Restaurant.objects.filter(manager=manager)
        return Category.objects.filter(restaurant__in=restaurant)


class ListMyFoodCategoryApiView(ListAPIView):
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated, IsManager]
    query = 'SELECT * FROM restaurants_category WHERE restaurant_id IN (' \
            'SELECT id FROM restaurants_restaurant WHERE manager_id = (' \
            'SELECT id FROM authentications_manager WHERE user_id = %s))'

    def get_queryset(self):
        return Category.objects.raw(self.query, [self.request.user.id])


class ListMyRestaurantFoodCategoryApiView(ListAPIView):
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated, IsManager]
    query_category = 'SELECT * FROM restaurants_category WHERE restaurant_id = %s'
    query_restaurant = 'SELECT id FROM restaurants_restaurant WHERE manager_id = (' \
                       'SELECT id FROM authentications_manager WHERE user_id = %s))'

    def get_queryset(self, id):
        manager = Manager.objects.get(user=self.request.user)
        restaurant = Restaurant.objects.filter(manager=manager, id=id)
        return Category.objects.filter(restaurant__in=restaurant)

    def list(self, request, *args, **kwargs):
        try:
            categories = self.get_queryset(self.kwargs['pk'])
            serializer = CategorySerializer(categories, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except:
            message = {'restaurant_id': kwargs['pk'], 'message': "Restaurant with this id doesn't exist in your restaurant list"}
            return Response(message, status=status.HTTP_400_BAD_REQUEST)



"------------------------- Food Views -------------------------"


class ListFoodApiView(ListAPIView):
    serializer_class = FoodSerializer
    queryset = Food.objects.all()


class ListFoodByCategoryApiView(ListAPIView):
    serializer_class = FoodSerializer

    def list(self, request, *args, **kwargs):
        queryset = Food.objects.filter(category=self.kwargs['pk'])
        serializer = FoodSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ListFoodByRestaurantApiView(ListAPIView):
    serializer_class = FoodSerializer

    def list(self, request, *args, **kwargs):
        queryset = Food.objects.filter(restaurant=self.kwargs['pk'])
        serializer = FoodSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class GetFoodApiView(RetrieveAPIView):
    serializer_class = FoodSerializer
    queryset = Food.objects.all()


'------------ For Admin ------------'


class AddFoodApiView(CreateAPIView):
    serializer_class = FoodSerializer
    permission_classes = [IsAuthenticated, IsManager]



class Get_Update_Delete_My_FoodApiView(RetrieveUpdateDestroyAPIView):
    serializer_class = FoodSerializer
    permission_classes = [IsAuthenticated, IsManager]

    def get_queryset(self):
        manager = Manager.objects.get(user=self.request.user)
        restaurant = Restaurant.objects.filter(manager=manager)
        return Food.objects.filter(restaurant__in=restaurant)
    
class ListMyFoodApiView(ListAPIView):
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated, IsManager]
    query = 'SELECT * FROM restaurants_food WHERE restaurant_id IN (' \
            'SELECT id FROM restaurants_restaurant WHERE manager_id = (' \
            'SELECT id FROM authentications_manager WHERE user_id = %s))'

    def get_queryset(self):
        return Food.objects.raw(self.query, [self.request.user.id])


"------------------------- Review Views -------------------------"


class ListReviewsByRestaurantApiView(ListAPIView):
    serializer_class = ReviewSerializer

    def list(self, request, *args, **kwargs):
        queryset = Review.objects.filter(restaurant=self.kwargs['pk'])
        serializer = ReviewSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class GetReviewApiView(RetrieveAPIView):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()


class ListMyReviewsApiView(ListAPIView):
    serializer_class = ReviewSerializer
    permission_classes = [IsAuthenticated]
    query = 'SELECT * FROM restaurants_review WHERE user_id = %s'

    def get_queryset(self):
        return Review.objects.raw(self.query, [self.request.user.id])


class Recommend_Model:
    def predict(text):
        nltk.download('stopwords')
        review = re.sub('[^a-zA-Z]', ' ', text)
        review = review.lower()
        review = review.split()

        ps = PorterStemmer()

        # stemming
        review = [ps.stem(word) for word in review if not word in set(stopwords.words('english'))]

        review = ' '.join(review)

        # load the CountVectorizer, StandardScaler and Model from disk

        cv = joblib.load(open("model/vector.pickel", "rb"))
        test_data_features = cv.transform([review])
        sc = joblib.load(open("model/StandardScaler.pickel", "rb"))
        test_data_features = sc.transform(test_data_features)
        loaded_model = joblib.load("model/model.pickle")
        result = loaded_model.predict(test_data_features)[0]
        if result == 0:
            return "Negative"
        else:
            return "Positive"


class AddReviewApiView(CreateAPIView):
    serializer_class = ReviewSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        serializer.save(type=Recommend_Model.predict(self.request.data['body']))


class Get_Update_Delete_My_ReviewApiView(RetrieveUpdateDestroyAPIView):
    serializer_class = ReviewSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Review.objects.filter(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(type=Recommend_Model.predict(self.request.data['body']))
