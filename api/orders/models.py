from django.db import models
from authentications.models import User
from restaurants.models import Food


class Order(models.Model):
    choices = (
    ('Shipped', 'Shipped'),
    ('In Progress', 'In Progress'),
    )

    total = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.IntegerField(blank=True, default=1)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    user = models.ForeignKey(User, related_name="user", on_delete=models.CASCADE)
    food = models.ForeignKey(Food, related_name="food", on_delete=models.CASCADE)
    status = models.CharField(max_length=100, choices=choices, default="In Progress", blank=True)



