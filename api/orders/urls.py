from django.urls import path
from .views import *


urlpatterns = [
    #     "------------------------- Order urls -------------------------"

    path('add-order/', AddOrderApiView.as_view(), name='add-order'),
    path('orders/me/', ListMyOrderApiView.as_view(), name='my-orders'),
    path('orders/<int:pk>/', Get_Update_Delete_My_OrderApiView.as_view(), name='get-update-delete-my-order'),
    path('orders/search/', Search_In_My_Orders.as_view(), name='search-in-my-order'),
]