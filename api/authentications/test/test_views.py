from .test_setup import TestSetUp
from rest_framework import status
from django.urls import reverse
import json


class NewUserSignupTestCase(TestSetUp):
    def test_new_user_can_signup(self):
        response = self.client.post(self.signup_url, self.user_data, format='json')
        # import pdb
        # pdb.set_trace()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class GetAuthUserTestCase(TestSetUp):
    def test_get_authenticated_data(self):
        response = self.client.get(self.auth_url)
        # import pdb
        # pdb.set_trace()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['email'], "testuser2@email.com")

    def test_authenticated_manager(self):
        data = {
                'name': 'test manager',
                'user': self.user
                }
        response = self.client.post(reverse('create-manager'), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_unauthenticated_user_cannot_get_manager_list(self, user=None, Token=None):
        self.client.force_authenticate(user=None, token=None)
        response = self.client.get(reverse('manager-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_profile_retrieve(self):
        response = self.client.get(reverse("user-profile", kwargs={'pk': 5}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_user_profile(self):
        data = {
            "id": 5,
            "image": "",
            "phone": "",
            "country": "morocco",
            "city": "rabat",
            "user": 4
        }
        response = self.client.put(reverse("user-profile", kwargs={'pk': 5}), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
