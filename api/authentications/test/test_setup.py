from rest_framework.test import APITestCase
from ..models import User
from rest_framework.authtoken.models import Token


class TestSetUp(APITestCase):

    def setUp(self):
        self.signup_url = '/api/auth/users/'
        self.auth_url = '/api/auth/users/me/'
        self.user_data = {
            'email': 'testuser@email.com',
            'username': 'TestUser',
            'first_name': 'test',
            'last_name': 'test',
            'password': 'anssari1234',
            're_password': 'anssari1234'
        }

        self.user = User.objects.create_user(email="testuser2@email.com", username="test2", password="test234")
        self.token = Token.objects.create(user=self.user)
        self.api_authorization()

    def api_authorization(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

        return super().setUp()

    def tearDown(self):
        return super().tearDown()
