from rest_framework.views import APIView
from .models import Profile, User, Manager
from .serializers import ProfileSerializer, ManagerSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import FormParser, JSONParser, MultiPartParser
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView, RetrieveUpdateDestroyAPIView
from .permissions import IsManager, IsAlreadyCreated


class UserProfileDetailUpdate(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    parser_classes = [FormParser, JSONParser, MultiPartParser]

    def get(self, request, *args, **kwargs):
        profile = Profile.objects.get(user=request.user)
        serializer = ProfileSerializer(profile)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(selfself, request, *args, **kwargs):
        profile = Profile.objects.get(user=request.user)
        serializer = ProfileSerializer(profile, request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.error, status=status.HTTP_400_BAD_REQUEST)


class CreateManagerApiView(CreateAPIView):
    serializer_class = ManagerSerializer
    queryset = Manager.objects.all()
    permission_classes = [IsAuthenticated, IsAlreadyCreated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        user_obj = self.request.user
        user_obj.is_manager = True
        user_obj.save()


class ManagersListApiView(ListAPIView):
    serializer_class = ManagerSerializer
    queryset = Manager.objects.all()


class ManagerApiView(APIView):
    serializer_class = ManagerSerializer
    #     queryset = Manager.objects.all

    def get(self, request, pk):
        if Manager.objects.filter(id=pk).exists():
            manager = Manager.objects.get(id=pk)
            serializer = ManagerSerializer(manager)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            response = {'id': pk, 'message': "Manager with this id doesn't exist"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)


class MyManagerApiView(APIView):
    serializer_class = ManagerSerializer
    permission_classes = [IsAuthenticated, IsManager]

    def get(self, request):
        manager = Manager.objects.get(user=request.user)
        serializer = ManagerSerializer(manager)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request):
        manager = Manager.objects.get(user=request.user)
        serializer = ManagerSerializer(manager, request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        manager = Manager.objects.get(user=request.user)
        manager.delete()
        return Response("message: Manager role deleted from this user successfully", status=status.HTTP_204_NO_CONTENT)
