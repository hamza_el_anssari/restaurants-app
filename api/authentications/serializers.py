from rest_framework import serializers
from djoser.serializers import UserSerializer, UserCreateSerializer
from .models import User, Profile, Manager
from restaurants.serializers import *


class UserCreateSerializer(UserCreateSerializer):

    class Meta(UserCreateSerializer.Meta):
        model = User
        fields = ['id', 'email', 'username', 'first_name', 'last_name']


class User_Serializer(UserSerializer):
    class Meta(UserSerializer.Meta):
        model = User
        fields = '__all__'


class ProfileSerializer(serializers.ModelSerializer):
    user = User_Serializer(read_only=True)
    reviews = ReviewSerializer(many=True, read_only=True)

    class Meta:
        model = Profile
        fields = ['id', 'phone', 'city', 'country', 'user', 'reviews']

        def update(self, instance, validated_data):
            instance.phone = validated_data.get('phone', instance.phone)
            instance.city = validated_data.get('city', instance.city)
            instance.country = validated_data.get('country', instance.country)
            instance.save()
            return instance


class ManagerSerializer(serializers.ModelSerializer):
    user = User_Serializer(read_only=True)
    restaurants = RestaurantSerializer(many=True, read_only=True)

    class Meta:
        model = Manager
        fields = ['id', 'restaurants', 'user']
        read_only_fields = ['user']
