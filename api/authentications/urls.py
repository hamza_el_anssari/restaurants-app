from django.urls import path, include
from .views import *


urlpatterns = [
    #     "------------------------- User urls -------------------------"

    path('', include('djoser.urls')),
    path('', include('djoser.urls.authtoken')),
    path('user/profile/', UserProfileDetailUpdate.as_view(), name='user-profile'),
    path('create-manager/', CreateManagerApiView.as_view(), name='create-manager'),

    #     "------------------------- Manager urls -------------------------"
    path('managers/', ManagersListApiView.as_view(), name='managers'),
    path('managers/<int:pk>/', ManagerApiView.as_view(), name='manager'),
    path('manager/me/', MyManagerApiView.as_view(), name='manager')

]
