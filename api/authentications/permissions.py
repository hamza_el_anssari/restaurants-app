from rest_framework import permissions


class IsManager(permissions.BasePermission):
    message = "You don't have permissions, this operation is not allowed for users"

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_manager)


class IsAlreadyCreated(permissions.BasePermission):
    message = "This account is already registered as manager account"

    def has_permission(self, request, view):
        return not bool(request.user and request.user.is_manager)
