from django.contrib import admin
from .models import User, Profile, Manager

admin.site.register(User)
admin.site.register(Profile)
admin.site.register(Manager)