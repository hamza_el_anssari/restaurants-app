FROM python:3.7-alpine


ENV PYTHONUNBUFFERED 1


RUN mkdir /api
WORKDIR /api

COPY ./api/requirements.txt /api/requirements.txt
RUN apk add --update --no-cache postgresql-client
RUN apk add --update --no-cache --virtual .tmp-build-deps \
    gcc libc-dev linux-headers postgresql-dev make git  \
    libffi-dev openssl-dev python3-dev libxml2-dev libxslt-dev

RUN pip install --upgrade pip

RUN pip --no-cache-dir install -r /api/requirements.txt

RUN apk del .tmp-build-deps

COPY ./api /api

RUN adduser -D user
USER user